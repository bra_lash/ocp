<!doctype html>
<html>
    <head>
        <title>Dashboard | Online Course Portal</title>
        <link rel="stylesheet" href="../css/admin.css" />
        <link rel="stylesheet" href="../css/entypo/css/entypo.css" />
        <link rel="stylesheet" href="../css/semantic.css">
    </head>
    <body>
        <section class="wrapper">
            <aside class="menu">
                <div class="logo"></div>
                <ul>
                    <li>
                        <a href="dashboard.php">
                            <span><i class="entypo-users"></i></span> Students
                        </a>
                    </li>
                    <li>
                        <a href="courses.php">
                            <span><i class="entypo-docs"></i></span> Courses
                        </a>
                    </li>
                    <li>
                        <a href="lecturer.php">
                            <span><i class="entypo-briefcase"></i></span> Lecturers
                        </a>
                    </li>
                </ul>  
            </aside>
            <article class="content">
                <h1 class="ui header">
					<i class="entypo-docs"></i> Courses
					<div class="sub header">Manage Courses Data</div>
				</h1>
                
                <div class="ui hidden divider"></div>
                
                <div class="ui four statistics">
                    <div class="statistic">
                        <div class="value">
                            200
                        </div>
                        <div class="label">
                            Students
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Lecturers
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            300
                        </div>
                        <div class="label">
                            Announcements
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Course Materials
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
                
                
                <div class="ui header">All Courses</div>
                <span class="ui button blue" id="new-course"><i class="entypo-plus"></i> New Course</span>
                <div class="ui segment">
                    <table class="ui table celled sortable">
                        <thead>
                            <tr>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Description</th>
                                <th>Lecturer</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Calculus</td>
                                <td>MA 450</td>
                                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati. Dignissimos assumenda provident architecto deserunt accusantium ea facilis molestiae optio. </td>
                                <td>Mr. Mensah</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                            <tr>
                                <td>Advanced Maths</td>
                                <td>MA 450</td>
                                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati. Dignissimos assumenda provident architecto deserunt accusantium ea facilis molestiae optio. </td>
                                <td>Mr. Ansah</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>
        
        
        
        <!-- Add Course Modal -->
		<div class="ui small modal new-course">
			<i class="close icon"></i>
			<div class="header blue">
				<i class="icon-plus"></i> <span class="sn">New Course</span>
			</div>
			<div class="content">
				<form class="ui form">
					<div class="field">
						<label>Course Name</label>
						<input type="text" name="course_name" />
					</div>
					<div class="field">
						<label>Course Code</label>
						<input type="text" name="course_code" />
					</div>
					<div class="field">
						<label>Description</label>
						<input type="text" name="desc" />
					</div>
					<div class="field">
						<label>Lecturer</label>
						<select name="lect" id="">
						    <option value="">Mr. Mensah</option>
						    <option value="">Mr. Ansah</option>
						    <option value="">Mr. Moses</option>
						    <option value="">Mr. John</option>
						    <option value="">Mr. Mahama</option>
						</select>
					</div>
				</form>	
			</div>
		  <div class="actions">
				<div class="ui red button cncl">Cancel</div>
				<div class="ui blue button">Save</div>
		  </div>
		</div>
        
        <script src="../js/jquery.js"></script>
        <script src="../js/semantic.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/admin.js"></script>
    </body>
</html>