<!doctype html>
<html>
    <head>
        <title>Dashboard | Online Course Portal</title>
        <link rel="stylesheet" href="../css/admin.css" />
        <link rel="stylesheet" href="../css/entypo/css/entypo.css" />
        <link rel="stylesheet" href="../css/semantic.css">
    </head>
    <body>
        <section class="wrapper">
            <aside class="menu">
                <div class="logo"></div>
                <ul>
                    <li>
                        <a href="dashboard.php">
                            <span><i class="entypo-users"></i></span> Students
                        </a>
                    </li>
                    <li>
                        <a href="courses.php">
                            <span><i class="entypo-docs"></i></span> Courses
                        </a>
                    </li>
                    <li>
                        <a href="lecturer.php">
                            <span><i class="entypo-briefcase"></i></span> Lecturers
                        </a>
                    </li>
                </ul>  
            </aside>
            <article class="content">
                <h1 class="ui header">
					<i class="entypo-briefcase"></i> Lecturers
					<div class="sub header">Manage Lecturer Data</div>
				</h1>
                
                <div class="ui hidden divider"></div>
                
                <div class="ui four statistics">
                    <div class="statistic">
                        <div class="value">
                            200
                        </div>
                        <div class="label">
                            Students
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Lecturers
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            300
                        </div>
                        <div class="label">
                            Announcements
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Course Materials
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
                
                
                <div class="ui header">All Lecturers</div>
                <span class="ui button blue" id="new-lect"><i class="entypo-plus"></i> New Lecturer</span>
                <div class="ui segment">
                    <table class="ui table celled sortable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Password</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Joshua Ato</td>
                                <td>5555555555</td>
                                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, obcaecati. Dignissimos assumenda provident architecto deserunt accusantium ea facilis molestiae optio. Numquam, assumenda iste fuga doloribus quod voluptates culpa. Maiores, laborum.</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                            <tr>
                                <td>Jane Mensah</td>
                                <td>5555555555</td>
                                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est dolores saepe fugit accusantium consectetur nobis sunt quisquam sequi neque, expedita omnis natus dignissimos rem esse iste provident, accusamus. Placeat, alias.</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>
        
        
        
        <!-- Add Lecturer Modal -->
		<div class="ui small modal new-lect">
			<i class="close icon"></i>
			<div class="header blue">
				<i class="icon-plus"></i> <span class="sn">New Lecturer</span>
			</div>
			<div class="content">
				<form class="ui form">
					<div class="field">
						<label>Lecturer Name</label>
						<input type="text" name="lect_name" />
					</div>
					<div class="field">
						<label>Password</label>
						<input type="text" name="pass" />
					</div>
					<div class="field">
						<label>Confirm Password</label>
						<input type="text" name="conf_pass" />
					</div>
					<div class="field">
						<label>Description</label>
						<textarea name="desc" id="" cols="30" rows="10"></textarea>
					</div>
				</form>	
			</div>
		  <div class="actions">
				<div class="ui red button cncl">Cancel</div>
				<div class="ui blue button">Save</div>
		  </div>
		</div>
        
        <script src="../js/jquery.js"></script>
        <script src="../js/semantic.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/admin.js"></script>
    </body>
</html>