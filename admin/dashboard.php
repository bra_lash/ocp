<!doctype html>
<html>
    <head>
        <title>Dashboard | Online Course Portal</title>
        <link rel="stylesheet" href="../css/admin.css" />
        <link rel="stylesheet" href="../css/entypo/css/entypo.css" />
        <link rel="stylesheet" href="../css/semantic.css">
    </head>
    <body>
        <section class="wrapper">
            <aside class="menu">
                <div class="logo"></div>
                <ul>
                    <li>
                        <a href="dashboard.php">
                            <span><i class="entypo-users"></i></span> Students
                        </a>
                    </li>
                    <li>
                        <a href="courses.php">
                            <span><i class="entypo-docs"></i></span> Courses
                        </a>
                    </li>
                    <li>
                        <a href="lecturer.php">
                            <span><i class="entypo-briefcase"></i></span> Lecturers
                        </a>
                    </li>
                </ul>  
            </aside>
            <article class="content">
                <h1 class="ui header">
					<i class="entypo-users"></i> Students
					<div class="sub header">Manage Student Data</div>
				</h1>
                
                <div class="ui hidden divider"></div>
                
                <div class="ui four statistics">
                    <div class="statistic">
                        <div class="value">
                            200
                        </div>
                        <div class="label">
                            Students
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Lecturers
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            300
                        </div>
                        <div class="label">
                            Announcements
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Course Materials
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
                
                
                <div class="ui header">All Students</div>
                <span class="ui button blue" id="new-stud"><i class="entypo-plus"></i> New Student</span>
                <div class="ui segment">
                    <table class="ui table celled sortable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Reference Number</th>
                                <th>Index Number</th>
                                <th>Year</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Joshua Ato</td>
                                <td>5555555555</td>
                                <td>555555555566</td>
                                <td>2012</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                            <tr>
                                <td>Jane Mensah</td>
                                <td>5555555555</td>
                                <td>555555555566</td>
                                <td>2012</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>
        
        
        
        <!-- Add Student Modal -->
		<div class="ui small modal new-stud">
			<i class="close icon"></i>
			<div class="header blue">
				<i class="icon-plus"></i> <span class="sn">New Student</span>
			</div>
			<div class="content">
				<form class="ui form">
					<div class="field">
						<label>Student Name</label>
						<input type="text" name="stud_name" />
					</div>
					<div class="field">
						<label>Reference Number</label>
						<input type="text" name="ref_num" />
					</div>
					<div class="field">
						<label>Index Number</label>
						<input type="text" name="index_num">
					</div>
					<div class="field">
						<label>Year of Admission</label>
						<input type="text" name="year" />
					</div>
				</form>	
			</div>
		  <div class="actions">
				<div class="ui red button cncl">Cancel</div>
				<div class="ui blue button">Save</div>
		  </div>
		</div>
        
        <script src="../js/jquery.js"></script>
        <script src="../js/semantic.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/admin.js"></script>
    </body>
</html>