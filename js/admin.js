$(document).ready(function() {
    $('#new-stud').on('click', function() {
        $('.ui.modal.new-stud').modal('show');
    });
    $('#new-lect').on('click', function() {
        $('.ui.modal.new-lect').modal('show');
    });
    $('#new-course').on('click', function() {
        $('.ui.modal.new-course').modal('show');
    });
    
    $('.cncl').on('click', function() {
        $('.ui.modal').modal('hide');
    });
});