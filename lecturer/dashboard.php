<!doctype html>
<html>
    <head>
        <title>Dashboard | Online Course Portal</title>
        <link rel="stylesheet" href="../css/admin.css" />
        <link rel="stylesheet" href="../css/entypo/css/entypo.css" />
        <link rel="stylesheet" href="../css/semantic.css">
    </head>
    <body>
        <section class="wrapper">
            <aside class="menu">
                <div class="logo"></div>
                <ul>
                    <li>
                        <a href="dashboard.php">
                            <span><i class="entypo-upload"></i></span> Uploads
                        </a>
                    </li>
                    <li>
                        <a href="announcements.php">
                            <span><i class="entypo-bell"></i></span> Announcements
                        </a>
                    </li>
                </ul>  
            </aside>
            <article class="content">
                <h1 class="ui header">
					<i class="entypo-upload"></i> Uploads
					<div class="sub header">Upload Assignments and Lecture Materials</div>
				</h1>
                
                <div class="ui hidden divider"></div>
                
                <div class="ui four statistics">
                    <div class="statistic">
                        <div class="value">
                            200
                        </div>
                        <div class="label">
                            Students
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Lecturers
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            300
                        </div>
                        <div class="label">
                            Announcements
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Course Materials
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
                
                
                <div class="ui header">All Uploads</div>
                <span class="ui button blue" id="new-stud"><i class="entypo-plus"></i> New Upload</span>
                <div class="ui segment">
                    <table class="ui table celled sortable">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Class</th>
                                <th>File</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Assignment</td>
                                <td>MA 3</td>
                                <td>res/calculus.pdf</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                            <tr>
                                <td>Lecture Material</td>
                                <td>MA 2</td>
                                <td>res/statistics.pdf</td>
                                <td class="collapsing">
									<button class="ui icon edit-sch button blue">
										<i class="icon pencil"></i>
									</button>
									<button class="ui icon button red">
										<i class="entypo-cancel"></i>
									</button>
								</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
        </section>
        
        
        
        <!-- Add Student Modal -->
		<div class="ui small modal new-stud">
			<i class="close icon"></i>
			<div class="header blue">
				<i class="icon-plus"></i> <span class="sn">New Upload</span>
			</div>
			<div class="content">
				<form class="ui form">
					<div class="field">
						<label>Upload Type</label>
						<select name="" id="">
						    <option value="">Assignment</option>
						    <option value="">Lecture Material</option>
						</select>
					</div>
					<div class="field">
						<label>Class</label>
						<select name="" id="">
						    <option value="">MA 1</option>
						    <option value="">MA 2</option>
						    <option value="">MA 3</option>
						    <option value="">MA 4</option>
						</select>
					</div>
					<div class="field">
						<label>Choose File</label>
						<input type="file" name="index_num">
					</div>
				</form>	
			</div>
		  <div class="actions">
				<div class="ui red button cncl">Cancel</div>
				<div class="ui blue button">Upload</div>
		  </div>
		</div>
        
        <script src="../js/jquery.js"></script>
        <script src="../js/semantic.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/admin.js"></script>
    </body>
</html>