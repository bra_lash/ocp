<!doctype html>
<html>
    <head>
        <title>Announcements | Online Course Portal</title>
        <link rel="stylesheet" href="../css/admin.css" />
        <link rel="stylesheet" href="../css/entypo/css/entypo.css" />
        <link rel="stylesheet" href="../css/semantic.css">
    </head>
    <body>
        <section class="wrapper">
            <aside class="menu">
                <div class="logo"></div>
                <ul>
                    <li>
                        <a href="dashboard.php">
                            <span><i class="entypo-upload"></i></span> Uploads
                        </a>
                    </li>
                    <li>
                        <a href="announcements.php">
                            <span><i class="entypo-bell"></i></span> Announcements
                        </a>
                    </li>
                    <li>
                        <a href="discussion.php">
                            <span><i class="entypo-chat"></i></span> Discussion
                        </a>
                    </li>
                </ul>  
            </aside>
            <article class="content">
                <h1 class="ui header">
					<i class="entypo-bell"></i> Announcements
					<div class="sub header">Add and Manage Announcements</div>
				</h1>
                
                <div class="ui hidden divider"></div>
                
                <div class="ui four statistics">
                    <div class="statistic">
                        <div class="value">
                            200
                        </div>
                        <div class="label">
                            Students
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Lecturers
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            300
                        </div>
                        <div class="label">
                            Announcements
                        </div>
                    </div>
                    <div class="statistic">
                        <div class="value">
                            20
                        </div>
                        <div class="label">
                            Course Materials
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
                
                
                <div class="ui header">All Announcements</div>
                <span class="ui button blue" id="new-stud" style="display: block; width: 200px;">
                    <i class="entypo-plus"></i> New Announcement
                </span>
                <!--Announcements-->
                
                <div class="ann">
                <div class="ui card">
                    <div class="content">
                        <div class="header">Announcement Title</div>
                        <div class="meta">22nd February 2016</div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ex cum cupiditate pariatur incidunt doloremque quas obcaecati temporibus necessitatibus est ratione animi eos ad, blanditiis a nesciunt quo, debitis! Vel.
                        </div>
                    </div>
                    <div class="extra content">
                       <div class="left floated author">
                           MA 2
                       </div>
                        <div class="right floated author">
                            Mr. Aminsah
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="ann">
                <div class="ui card">
                    <div class="content">
                        <div class="header">Announcement Title</div>
                        <div class="meta">22nd February 2016</div>
                        <div class="description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ex cum cupiditate pariatur incidunt doloremque quas obcaecati temporibus necessitatibus est ratione animi eos ad, blanditiis a nesciunt quo, debitis! Vel.
                        </div>
                    </div>
                    <div class="extra content">
                       <div class="left floated author">
                           MA 2
                       </div>
                        <div class="right floated author">
                            Mr. Aminsah
                        </div>
                    </div>
                </div>
                </div>
            </article>
        </section>
        
        
        
        <!-- Add Student Modal -->
		<div class="ui small modal new-stud">
			<i class="close icon"></i>
			<div class="header blue">
				<i class="icon-plus"></i> <span class="sn">New Announcement</span>
			</div>
			<div class="content">
				<form class="ui form">
					<div class="field">
						<label>Title</label>
						<input type="text" name="title" />
					</div>
					<div class="field">
						<label>Date</label>
						<input type="date" name="date" />
					</div>
					<div class="field">
						<label>Announcement</label>
						<textarea name="body" id="" cols="30" rows="10"></textarea>
					</div>
					<div class="field">
						<label>Class</label>
						<select name="" id="">
						    <option value="">MA 1</option>
						    <option value="">MA 2</option>
						    <option value="">MA 3</option>
						    <option value="">MA 4</option>
						</select>
					</div>
				</form>	
			</div>
		  <div class="actions">
				<div class="ui red button cncl">Cancel</div>
				<div class="ui blue button">Save</div>
		  </div>
		</div>
        
        <script src="../js/jquery.js"></script>
        <script src="../js/semantic.js"></script>
        <script src="../js/jquery.dataTables.js"></script>
        <script src="../js/admin.js"></script>
    </body>
</html>